DOCKERREPO = 'polyglot'.freeze
NAME = 'npm-graphql'.freeze
VERSION='0.1.0'.freeze

require 'bundler/setup'
require 'sinatra/cross_origin'
require 'graphql/schema/npm'
require 'json'
require 'yaml'

require './app'
