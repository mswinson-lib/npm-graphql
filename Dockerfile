FROM ruby:latest

ADD . /var/services/npm-graphql
WORKDIR /var/services/npm-graphql

RUN ./scripts/setup
CMD ./scripts/init
