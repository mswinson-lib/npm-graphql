module App
  class API < Sinatra::Base

    register Sinatra::CrossOrigin

    configure do
      set :config, settings.root + '/config'
    end

    # configure datastore
    configure :development do
      yaml_data = YAML.load_file("#{settings.config}/db.yml")
      db_data = yaml_data['development']

      dbtype = ENV['DBTYPE'] || db_data['type']
      dbuser = ENV['DBUSER'] || db_data['user']
      dbhost = ENV['DBHOST'] || db_data['host']
      dbport = ENV['DBPORT'] || db_data['port']
      dbname = ENV['DBNAME'] || db_data['name']

      dburi = URI::HTTPS.build(:host => dbhost, :port => dbport,  :user => dbuser, :path => dbname)
      set :dburl, dburi.to_s
    end

    configure :production do
      yaml_data = YAML.load_file("#{settings.config}/db.yml")
      db_data = yaml_data['production']

      dbtype = ENV['DBTYPE'] || db_data['type']
      dbuser = ENV['DBUSER'] || db_data['user']
      dbhost = ENV['DBHOST'] || db_data['host']
      dbport = ENV['DBPORT'] || db_data['port']
      dbname = ENV['DBNAME'] || db_data['name']

      dburi = URI::HTTPS.build(:host => dbhost, :port => dbport,  :user => dbuser, :path => dbname)
      set :dburl, dburi.to_s
    end


    configure :development, :production do
      dburl = settings.dburl
      uri = URI(dburl)
      host = uri.host

      GraphQL::Schemas::NPM.configure do |config|
        config.url = dburl
      end
    end

    # configure web server
    configure :development, :production do
      set :server, %w[thin mongrel webrick]
      set :bind, '0.0.0.0'
      set :port, 5000

      enable :logging
    end

    configure :development do
      enable :cross_origin
      set :allow_origin, :any
      set :allow_methods, %i[get post options]
      set :allow_credentials, true
      set :max_age, '1728000'
      set :expose_headers, ['Content-Type']

      set :show_exceptions, :after_handler
    end

    # conditions
    set(:check_env) { |value| condition{value.to_s == settings.environment.to_s} }

    not_found do
      'resource not found'
    end

    error do
      'error'
    end

    options '*' do
      response.headers["Allow"] = "HEAD,GET,PUT,POST,DELETE,OPTIONS"

      response.headers["Access-Control-Allow-Headers"] = "X-Requested-With, X-HTTP-Method-Override, Content-Type, Cache-Control, Accept"
      200
    end

    get '/config/?' , :check_env => 'development' do
      content_type 'application/json'

      config = {
        'registry' => {
          'url' => settings.dburl
        }
      }

      status 200
      body JSON.pretty_generate(config)
    end

    get '/graphql/?' do
      content_type 'application/json'

      query = GraphQL::Introspection::INTROSPECTION_QUERY
      results = GraphQL::Types::NPM::Schema.execute(query)

      status 200
      body JSON.pretty_generate(results)
    end

    post '/graphql/?' do
      query = ''

      case request.content_type
      when 'application/graphql'
        query = request.body.read
      when 'application/json'
        body = request.body.read
        json = JSON.parse(body)
        query = json['query']
      else
        raise Sinatra::NotFound
      end

      results = GraphQL::Types::NPM::Schema.execute(query)

      content_type 'application/json'
      status 200
      body JSON.pretty_generate(results)
    end

    run! if app_file == $PROGRAM_NAME
  end
end
