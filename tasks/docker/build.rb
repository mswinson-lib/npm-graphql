namespace :docker do
  desc 'build docker image'
  task :build do
    case SERVER_ENV
    when 'production'
      system("docker-compose build npm-graphql")
    when 'develop'
      system("docker-compose build npm-graphql-dev")
    end
  end
end

