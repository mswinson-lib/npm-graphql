namespace :docker do
  desc 'push docker image'
  task :push do
    system("docker login -u #{ENV['DOCKER_REPO_USER']} -p #{ENV['DOCKER_REPO_PASSWORD']} #{ENV['DOCKER_REPO_URL']}")

    case SERVER_ENV
    when 'develop'
      system("docker tag #{DOCKERREPO}/#{NAME}:#{VERSION}-develop #{ENV['DOCKER_REPO_HOST']}/#{DOCKERREPO}/#{NAME}:#{VERSION}-develop")
      system("docker push #{ENV['DOCKER_REPO_HOST']}/#{DOCKERREPO}/#{NAME}:#{VERSION}-develop")
    end
  end
end

