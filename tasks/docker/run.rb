namespace :docker do
 desc 'run service'
  task :run do
    case SERVER_ENV
    when 'production'
      system("docker-compose up npm-graphql")
    when 'develop'
      system("docker-compose up npm-graphql-dev")
    end
  end
end
